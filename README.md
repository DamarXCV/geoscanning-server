# Setup

## Installation Steps

1. Set up Docker
2. run Docker containers
3. create Database by navigating to <server-addr>/database/create.php
4. add sample data by navigating to <server-addr>/database/addSampleData.php

- Default Port: 80

## Login

Default users:

| email                          | password                      |
|--------------------------------|-------------------------------|
| thomas.dielokomotive@gmail.com | krassesPW                     |
| baummann@hotmail.de            | Baum                          |
| kevin89@web.de                 | secret                        |
| hackerman@aol.com              | \|\\\|0T\_\/\-\\\_\|°\\\/\\\/ | <!--- |\|0T_/-\_|°\/\/ -->
| KF0815@gmail.com               | Fury4711                      |
