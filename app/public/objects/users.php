<?php
class User{
    // database connection and table name
    private $conn;
    private $table_name = "users";

    // object properties
    public $id;
    public $pw;
    public $fname;
    public $lname;
    public $alias;
    public $email;
    public $joined;
    public $country;
    public $hometown;
    public $desc;
    public $profile_image;
    public $codes_found;
    public $codes_hidden;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read user
    function read(){
        // select query
        $query = "SELECT
                    fname, lname, alias, email, joined, country, hometown, `desc`, profile_image,
                    (SELECT
                        count(*)
                    FROM
                        code_ratings cr
                    WHERE
                        cr.user_id = u.id) as codes_found,
                    (SELECT
                        count(*)
                    FROM
                        codes c
                    WHERE
                        c.user_id = u.id) as codes_hidden
                FROM
                    " . $this->table_name . " u
                WHERE
                    id = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->id));

        // bind values
        $stmt->bindParam(1, $this->id);

        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->fname       = $row['fname'];
        $this->lname       = $row['lname'];
        $this->alias       = $row['alias'];
        $this->email       = $row['email'];
        $this->joined      = $row['joined'];
        $this->country     = $row['country'];
        $this->hometown    = $row['hometown'];
        $this->desc        = html_entity_decode($row['desc']);
        $this->profile_image=$row['profile_image'];
        $this->codes_found = $row['codes_found'];
        $this->codes_hidden= $row['codes_hidden'];
    }

    // create user
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                        (`pwhash`, `fname`, `lname`, `alias`, `email`, `joined`, `country`, `hometown`, `desc`, `profile_image`)
                VALUES
                    (:pwhash, :fname, :lname, :alias, :email, :joined, :country, :hometown, :desc, :profile_image)";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->firstname=htmlspecialchars(strip_tags($this->fname));
        $this->lastname=htmlspecialchars(strip_tags($this->lname));
        $this->alias=htmlspecialchars(strip_tags($this->alias));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->joined=htmlspecialchars(strip_tags($this->joined));
        $this->country=htmlspecialchars(strip_tags($this->country));
        $this->hometown=htmlspecialchars(strip_tags($this->hometown));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $this->profile_image=htmlspecialchars(strip_tags($this->profile_image));

        // bind values
        $stmt->bindParam(":pwhash", $this->pw);
        $stmt->bindParam(":fname", $this->fname);
        $stmt->bindParam(":lname", $this->lname);
        $stmt->bindParam(":alias", $this->alias);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":joined", $this->joined);
        $stmt->bindParam(":country", $this->country);
        $stmt->bindParam(":hometown", $this->hometown);
        $stmt->bindParam(":desc", $this->desc);
        $stmt->bindParam(":profile_image", $this->profile_image);
    
        // execute query
        if($stmt->execute()){
            // read user id from database and set value to object
            $query = "SELECT id FROM ".$this->table_name." WHERE alias = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $this->alias);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $row['id'];

            return true;
        }
    
        return false;   
    }

    // update the user
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    fname = :fname,
                    lname = :lname,
                    alias = :alias,
                    email = :email,
                    country = :country,
                    hometown = :hometown,
                    `desc` = :desc,
                    profile_image = :profile_image
                WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->fname=htmlspecialchars(strip_tags($this->fname));
        $this->lname=htmlspecialchars(strip_tags($this->lname));
        $this->alias=htmlspecialchars(strip_tags($this->alias));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->country=htmlspecialchars(strip_tags($this->country));
        $this->hometown=htmlspecialchars(strip_tags($this->hometown));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $this->profile_image=htmlspecialchars(strip_tags($this->profile_image));
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':fname', $this->fname);
        $stmt->bindParam(':lname', $this->lname);
        $stmt->bindParam(':alias', $this->alias);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':country', $this->country);
        $stmt->bindParam(':hometown', $this->hometown);
        $stmt->bindParam(':desc', $this->desc);
        $stmt->bindParam(':profile_image', $this->profile_image);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            // read joined from database and set value to object
            $query = "SELECT joined FROM ".$this->table_name." WHERE id = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->joined = $row['joined'];

            return true;
        }
    
        return false;
    }

    // delete the user
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // read bearer token from header
    function auth(){
        $headers = null;
        $token = null;
        // read header
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        // header found -> retrieve token
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                $token = $matches[1];
            }
        }
        return $token;
    }

    // search for bearer token
    function checkToken($token){
        // token empty?
        if ($token == null)
            return false;

        $query = "SELECT `user_id`, `alias` FROM `sessions` s LEFT JOIN `users` u ON s.user_id = u.id WHERE `token` = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $token);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set user id and user name to matching result
        $this->id = $row['user_id'];
        $this->alias = $row['alias'];

        // true if token found
        return boolval($this->id);
    }

    // create a new token and read user id and user name
    function createToken(){
    
        // query
        $query = "INSERT INTO `sessions` (`user_id`, `token`) VALUES (:user_id, :token)";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $payload = json_encode(['user_id' => $this->id]);

        // Encode Header to Base64Url String
        $header = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $payload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $header . "." . $payload, 'Sup3R5ecREt!', true);

        // Encode Signature to Base64Url String
        $signature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create Token
        $token = $header . "." . $payload . "." . $signature;

        $stmt->bindParam(':user_id', $this->id);
        $stmt->bindParam(':token', $token);
    
        // execute query
        if($stmt->execute())
            return $token;
    
        return false;
    }

    // used for paging users
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name;
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }

    // verifies pw hash and reads user id
    public function checkPw($pw){

        // read id, password hash belonging to given email
        $query = "SELECT COUNT(*) as c, `id`, `pwhash` FROM " . $this->table_name . " WHERE `email` = :email";
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
        
        // bind new values
        $stmt->bindParam(':email', $this->email);
        $stmt->execute();

        // retrieve info
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row['c']) {
            // set user id
            $this->id = $row['id'];

            // true on password match; hash type retrievable from hash
            return password_verify($pw, $row['pwhash']);
        } else {
            return false;
        }
    }

    // creates pw hash
    public function hashPw($pw){
        return password_hash($pw, PASSWORD_BCRYPT); // always 60 char long
    }
}
?>