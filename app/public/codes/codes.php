<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/codes.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$code = new Code($db);
$user = new User($db);

// The request is using the GET method
// get codes
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        if (!empty($data->code_id)) {

            $code->fe_id = $data->code_id;

            $code->readOne();

            // set response code - 200 OK
            http_response_code(200);

            // return read code
            echo json_encode(array(
                "id"          => $code->fe_id,
                "date_hidden" => $code->hidden,
                "title"       => $code->title,
                "category"    => intval($code->cat_id),
                "dif"         => floatval($code->dif),
                "fun"         => floatval($code->fun),
                "owner_id"    => intval($code->user_id),
                "owner_name"  => $code->user_name,
                "long"        => floatval($code->long),
                "lat"         => floatval($code->lat),
                "desc"        => $code->desc,
                "hint"        => $code->hint,
                "is_bookmarked"=>$code->checkMark($user->id)
            ));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the POST method
// set a code
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = (object) $_POST;
    if($user->checkToken($user->auth())) {
        // make sure data is not empty
        if(
            !empty($data->title) &&
            !empty($data->category) &&
            !empty($data->long) &&
            !empty($data->lat)/* &&
            !empty($data->desc) &&
            !empty($data->hint)*/
        ){
        
            // set code property values
            $code->fe_id   = 'GC' . str_pad(strtoupper(strval(bin2hex(random_bytes(3)))), 6, "0", STR_PAD_LEFT);
            while($code->checkFeid())
                $code->fe_id   = 'GC' . str_pad(strtoupper(strval(bin2hex(random_bytes(3)))), 6, "0", STR_PAD_LEFT);
            $code->rand_id = strtoupper(strval(bin2hex(random_bytes(20))));
            while($code->checkRandid())
                $code->rand_id = strtoupper(strval(bin2hex(random_bytes(20))));
            $code->user_id = $user->id;
            $code->user_name=$user->alias;
            $code->cat_id  = $data->category;
            $code->hidden  = date('Y-m-d H:i:s');
            $code->title   = $data->title;
            $code->long    = $data->long;
            $code->lat     = $data->lat;
            $code->desc    = $data->desc;
            $code->hint    = $data->hint;
            $code->fun     = 0;
            $code->dif     = 0;

            // create the code
            if($code->create()){
        
                // set response code - 201 created
                http_response_code(201);
        
                // return created object
                echo json_encode(array(
                    "id"          => $code->fe_id,
                    "date_hidden" => $code->hidden,
                    "title"       => $code->title,
                    "category"    => $code->cat_id,
                    "dif"         => $code->dif,
                    "fun"         => $code->fun,
                    "owner_id"    => $code->user_id,
                    "owner_name"  => $code->user_name,
                    "long"        => $code->long,
                    "lat"         => $code->lat,
                    "desc"        => $code->desc,
                    "hint"        => $code->hint
                ));
            }
        
            // if unable to create the code, tell the user
            else{
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("message" => "Unable to create code."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the PATCH method
// update a code
elseif ($_SERVER['REQUEST_METHOD'] === 'PATCH') {
    parse_str(file_get_contents('php://input'), $data);
    $data = (object) $data;
    if($user->checkToken($user->auth())) {

        // make sure data is not empty
        if(
            !empty($data->id) &&
            !empty($data->title) &&
            !empty($data->category) &&
            !empty($data->long) &&
            !empty($data->lat)/* &&
            !empty($data->desc) &&
            !empty($data->hint)*/
        ){
        
            // set code property values
            $code->fe_id   = $data->id;
            $code->user_id = $user->id;
            $code->user_name=$user->alias;
            $code->cat_id  = $data->category;
            $code->title   = $data->title;
            $code->long    = $data->long;
            $code->lat     = $data->lat;
            $code->desc    = $data->desc;
            $code->hint    = $data->hint;
            
            // update the code
            if($code->update()){
        
                $code->readOne();

                // set response code - 200 OK
                http_response_code(200);
        
                // return created object
                echo json_encode(array(
                    "id"          => $code->fe_id,
                    "date_hidden" => $code->hidden,
                    "title"       => $code->title,
                    "category"    => $code->cat_id,
                    "dif"         => $code->dif,
                    "fun"         => $code->fun,
                    "owner_id"    => $code->user_id,
                    "owner_name"  => $code->user_name,
                    "long"        => $code->long,
                    "lat"         => $code->lat,
                    "desc"        => $code->desc,
                    "hint"        => $code->hint
                ));
            }
        
            // if unable to update the code, tell the user
            else{
        
                // set response code - 204 no content
                http_response_code(204);
        
                // tell the user
                echo json_encode(array("message" => "no content."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to update code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the DELETE method
// delete a code
elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        // set code id to be deleted
        $code->fe_id = $data->code_id;
        
        // delete the code
        if($code->delete()){
        
            // set response code - 200 ok
            http_response_code(200);
        
            // tell the user
            //echo json_encode(array("message" => "Code was deleted."));
        }
        
        // if unable to delete the code
        else{
        
            // set response code - 503 service unavailable
            http_response_code(503);
        
            // tell the user
            echo json_encode(array("message" => "Unable to delete code."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    return false;
}

?>