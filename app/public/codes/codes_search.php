<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/codes.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$code = new Code($db);
$user = new User($db);

// The request is using the GET method
// search codes by search term
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        if (!empty($data->term) &&
            !empty($data->number_of_items)/* &&
            !empty($data->long) &&
            !empty($data->lat)*/) {


            $keyword=$data->term;
            $noItems=$data->number_of_items;
            $long  = $data->long;
            $lat   = $data->lat;

            $stmt = $code->search($keyword, $noItems, $long, $lat);
            $num = $stmt->rowCount();

            // check if more than 0 record found
            if($num>0){
            
                // codes array
                $code_arr=array();
            
                // retrieve our table contents
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    extract($row);
            
                    $code_item=array(
                        "id"         => $fe_id,
                        "category"   => intval($cat_id),
                        "title"      => $title,
                        "dif"        => floatval($dif),
                        "fun"        => floatval($fun),
                        "long"       => floatval($long),
                        "lat"        => floatval($lat)
                    );
            
                    array_push($code_arr, $code_item);
                }
            
                // set response code - 200 OK
                http_response_code(200);
            
                // show code data in json format
                echo json_encode($code_arr);
            } else {
            
                // set response code - 204 No Data
                http_response_code(204);
            
                // tell the user no codes found
                echo json_encode(
                    array("message" => "No codes found.")
                );
            }
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    return false;
}

?>