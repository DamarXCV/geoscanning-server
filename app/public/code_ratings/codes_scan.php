<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/code_ratings.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$codeRating = new CodeRating($db);
$user = new User($db);

// The request is using the POST method
// create empty log for found code
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = (object) $_POST;
    if($user->checkToken($user->auth())) {
        // make sure data is not empty
        if(!empty($data->key)){
            $codeRating->user_id   = $user->id;
            $codeRating->user_name = $user->alias;
            
            // search for the key
            if($codeRating->searchKey($data->key)){
                
                
                if(!$codeRating->readOne())
                    $codeRating->createEmpty();

                // set response code - 201 created
                http_response_code(201);

                // tell the user
                echo json_encode(array(
                    "id" => intval($codeRating->id),
                    "code_id" => $codeRating->code_fe_id,
                    "owner_id" => intval($codeRating->user_id),
                    "owner_name" => $codeRating->user_name,
                    "date_found" => $codeRating->found,
                    "dif" => floatval($codeRating->dif),
                    "fun" => floatval($codeRating->fun),
                    "comment" => $codeRating->comment
                ));
            }
        
            // if unable to find the key, tell the user
            else{
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("message" => "Unable to create code."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    return false;
}

?>