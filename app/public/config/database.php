<?php
class Database{
    private $host = 'database';
    private $db   = 'geoscanning';
    private $user = 'geoscanning_api';
    private $pass = '436j5bk35g3j5bk';
    private $port = "3306";
    private $charset = 'utf8mb4';
    public  $conn;

    public function getConnection(){
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset;port=$this->port";
        $this->conn = null;

        try {
            $this->conn = new \PDO($dsn, $this->user, $this->pass);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $this->conn;
    }
}
?>