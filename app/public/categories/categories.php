<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/categories.php';

$database = new Database();
$db = $database->getConnection();

$category = new Category($db);

// The request is using the GET method
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $stmt = $category->read();

    $num = $stmt->rowCount();

    if($num>0){
    
        // codes array
        $category_arr=array();
    
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            extract($row);
    
            $category_item=array(
                "id"   => intval($id),
                "name" => $name
            );
    
            array_push($category_arr, $category_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show code data in json format
        echo json_encode($category_arr);
    } else {
    
        // set response code - 204 No Data
        http_response_code(204);
    
        // tell the user no codes found
        echo json_encode(
            array("message" => "No codes found.")
        );
    }
}

// The request is not using any known method
else {
    return false;
}

?>