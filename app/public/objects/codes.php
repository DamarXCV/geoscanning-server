<?php
class Code{
    // database connection and table name
    private $conn;
    private $table_name = "codes";

    // object properties
    public $id;
    public $fe_id;
    public $rand_id;
    public $user_id;
    public $user_name;
    public $cat_id;
    public $hidden;
    public $title;
    public $long;
    public $lat;
    public $desc;
    public $hint;
    public $fun;
    public $dif;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // used to read a single code
    function readOne(){
    
        // query to read single record
        $query =
            "SELECT
                c.id, c.fe_id, u.id as user_id, u.alias as user_name, c.cat_id as category, c.hidden, c.title, c.long, c.lat, c.desc, c.hint
            FROM
                " . $this->table_name . " c
                LEFT JOIN
                    users u
                        ON c.user_id = u.id
                WHERE
                    c.fe_id = ?
                LIMIT
                    1";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of code to be updated
        $stmt->bindParam(1, $this->fe_id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->id      = $row['id'];
        $this->fe_id   = $row['fe_id'];
        $this->hidden  = $row['hidden'];
        $this->title   = $row['title'];
        $this->cat_id  = $row['category'];
        $this->user_id = $row['user_id'];
        $this->user_name=$row['user_name'];
        $this->long    = $row['long'];
        $this->lat     = $row['lat'];
        $this->desc    = html_entity_decode($row['desc']);
        $this->hint    = html_entity_decode($row['hint']);

        // select all query
        $query =
            "SELECT
                IFNULL(AVG(cr.fun), 0) as fun, IFNULL(AVG(cr.dif), 0) as dif
            FROM
                " . $this->table_name . " c
                LEFT JOIN
                    code_ratings cr
                        ON cr.code_id = c.id
                WHERE
                    c.fe_id = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind id of code to be updated
        $stmt->bindParam(1, $this->fe_id);
    
        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->dif = $row['dif'];
        $this->fun = $row['fun'];
    }

    // read codes near to location
    function readGeo($width, $height){  // width, height in deg
        $clamp = false;
        // min long value to search codes from
        $wmin = $this->long - ($width / 2);
        // max long value to search codes from
        $wmax = $wmin + $width;
        // clamping
        if ($wmin < -180){$wmin += 360; $clamp = true;}
        if ($wmin >  180){$wmin -= 360; $clamp = true;}
        // min lat value to search codes from
        $hmin = $this->lat - ($height / 2);
        // clamping not supported
        //if ($hmin < -90){$hmin = -180 + $hmin; $hic = true;}
        // max lat value to search codes from
        $hmax = $hmin + $height;
        //if ($hmax >  90){$hmax = 180 - $hmax; $hac = true;}
        $cat  = !empty($this->cat_id);

        $query =
            "SELECT
                `fe_id`, `cat_id` , `title`, `long`, `lat`
            FROM
                " . $this->table_name . " ";
        if($clamp)
            $query .= "WHERE
                `long`
                    BETWEEN :wmin AND 180
            OR
                `long`
                    BETWEEN -180 AND :wmax ";
        else
            $query .= "WHERE
                `long`
                    BETWEEN :wmin AND :wmax ";
        $query .= "AND
                `lat`
                    BETWEEN (:hmin) AND (:hmax)";
        if ($cat)
            $query .= "
                AND
                    `cat_id` = :cat_id";
        $query .= "
            ORDER BY
                `title` DESC, fe_id
            LIMIT
                50";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(":wmin", $wmin);
        $stmt->bindParam(":wmax", $wmax);
        $stmt->bindParam(":hmin", $hmin);
        $stmt->bindParam(":hmax", $hmax);
        if($cat)
            $stmt->bindParam(":cat_id", $this->cat_id);
        
        // execute query
        $stmt->execute();

        return $stmt;
    }

    // read codes blonging to given user
    function readOwner($user_id, $age, $start, $noItems){

        // select all query
        $query =
            "SELECT
                c.fe_id, c.cat_id , c.title, c.long, c.lat, c.hidden,
                (SELECT
                    IFNULL(AVG(cr.fun), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as fun,
                (SELECT
                    IFNULL(AVG(cr.dif), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as dif
            FROM
                " . $this->table_name . " c
            WHERE
                c.user_id = :user_id
            AND
                hidden < :hidden
            ORDER BY
                c.hidden DESC, c.fe_id
            LIMIT
                :start, :noItems";
        
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        //sanitize
        $user_id=htmlspecialchars(strip_tags($user_id));
        $age=htmlspecialchars(strip_tags($age));
        $noItems=htmlspecialchars(strip_tags($noItems));
        $start=htmlspecialchars(strip_tags($start));


        // bind values
        $stmt->bindParam(":user_id", $user_id);
        $stmt->bindParam(":hidden",  $age);
        $stmt->bindParam(":noItems", $noItems, PDO::PARAM_INT);
        $stmt->bindParam(":start",   $start,   PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();

        return $stmt;
    }

    // read codes found by user
    function readFound($user_id, $age, $start, $noItems){

        // select all query
        $query =
            "SELECT
                c.fe_id, c.cat_id , c.title, c.long, c.lat,
                (SELECT
                    IFNULL(AVG(cr.fun), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as fun,
                (SELECT
                    IFNULL(AVG(cr.dif), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as dif
            FROM
                code_ratings cr
            LEFT JOIN
                " . $this->table_name . " c
                    ON cr.code_id = c.id
            WHERE
                cr.user_id = :user_id
            AND
                c.hidden < :hidden 
            ORDER BY
                c.hidden DESC, c.fe_id
            LIMIT
                :start, :noItems";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        //sanitize
        $user_id=htmlspecialchars(strip_tags($user_id));
        $age=htmlspecialchars(strip_tags($age));
        $noItems=htmlspecialchars(strip_tags($noItems));
        $start=htmlspecialchars(strip_tags($start));

        // bind values
        $stmt->bindParam(":user_id", $user_id);
        $stmt->bindParam(":hidden",  $age);
        $stmt->bindParam(":noItems", $noItems, PDO::PARAM_INT);
        $stmt->bindParam(":start",   $start,   PDO::PARAM_INT);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    // read bookmarked codes
    function readMarks($user_id, $age, $start, $noItems){

        // select all query
        $query =
            "SELECT
                c.fe_id, c.cat_id , c.title, c.long, c.lat,
                (SELECT
                    IFNULL(AVG(cr.fun), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as fun,
                (SELECT
                    IFNULL(AVG(cr.dif), 0)
                FROM
                    code_ratings cr
                WHERE
                    cr.code_id = c.id) as dif
            FROM
                code_bookmarks cb
            LEFT JOIN
                " . $this->table_name . " c
                    ON cb.code_id = c.id
            WHERE
                cb.user_id = :user_id
            AND
                hidden < :hidden
            ORDER BY
                hidden DESC, c.fe_id
            LIMIT
                :start, :noItems";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        //sanitize
        $user_id=htmlspecialchars(strip_tags($user_id));
        $age=htmlspecialchars(strip_tags($age));
        $noItems=htmlspecialchars(strip_tags($noItems));
        $start=htmlspecialchars(strip_tags($start));

        // bind values
        $stmt->bindParam(":user_id", $user_id);
        $stmt->bindParam(":hidden",  $age);
        $stmt->bindParam(":noItems", $noItems, PDO::PARAM_INT);
        $stmt->bindParam(":start",   $start,   PDO::PARAM_INT);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    // read random id from code
    function readRandId($user_id){

        // select all query
        $query =
            "SELECT
                count(*) as c, rand_id
            FROM
                " . $this->table_name . "
            WHERE
                fe_id = :fe_id
            AND
                user_id = :user_id";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->fe_id=htmlspecialchars(strip_tags($this->fe_id));

        // bind values
        $stmt->bindParam(':fe_id', $this->fe_id);
        $stmt->bindParam(':user_id', $user_id);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($row['c']) {
            $this->rand_id = $row['rand_id'];
        }
    }

    // create code
    function create(){

        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                        (`fe_id`, `rand_id`, `user_id`, `cat_id`, `title`, `long`, `lat`, `desc`, `hint`)
                VALUES
                    (:fe_id, :rand_id, :user_id, :cat_id, :title, :long, :lat, :desc, :hint)";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->fe_id=htmlspecialchars(strip_tags($this->fe_id));
        $this->rand_id=htmlspecialchars(strip_tags($this->rand_id));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->cat_id=htmlspecialchars(strip_tags($this->cat_id));
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->long=htmlspecialchars(strip_tags($this->long));
        $this->lat=htmlspecialchars(strip_tags($this->lat));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $this->hint=htmlspecialchars(strip_tags($this->hint));

        // bind values
        $stmt->bindParam(":fe_id", $this->fe_id);
        $stmt->bindParam(":rand_id", $this->rand_id);
        $stmt->bindParam(":user_id", $this->user_id);
        $stmt->bindParam(":cat_id", $this->cat_id);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":long", $this->long);
        $stmt->bindParam(":lat", $this->lat);
        $stmt->bindParam(":desc", $this->desc);
        $stmt->bindParam(":hint", $this->hint);
    
        // execute query
        if ($stmt->execute())
            return true;
    
        return false;
    }

    // create bookmark
    function createMark($user_id, $fe_id){

        // query to insert record
        $query = "INSERT INTO
                    code_bookmarks
                        (`user_id`, `code_id`)
                VALUES
                    (:user_id, (SELECT `id` FROM codes WHERE `fe_id` = :fe_id))";
    
        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $user_id=htmlspecialchars(strip_tags($user_id));
        $fe_id=htmlspecialchars(strip_tags($fe_id));

        // bind values
        $stmt->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam(":fe_id", $fe_id);
    
        // execute query
        if ($stmt->execute())
            return true;
    
        return false;
    }

    // update the code
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    `cat_id`=:cat_id,
                    `title`=:title,
                    `long`=:long,
                    `lat`=:lat,
                    `desc`=:desc,
                    `hint`=:hint
                WHERE
                    `fe_id`=:fe_id";
    

        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->cat_id=htmlspecialchars(strip_tags($this->cat_id));
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->long=htmlspecialchars(strip_tags($this->long));
        $this->lat=htmlspecialchars(strip_tags($this->lat));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $this->hint=htmlspecialchars(strip_tags($this->hint));
        $this->fe_id=htmlspecialchars(strip_tags($this->fe_id));
    
        // bind values
        $stmt->bindParam(":cat_id", $this->cat_id);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":long", $this->long);
        $stmt->bindParam(":lat", $this->lat);
        $stmt->bindParam(":desc", $this->desc);
        $stmt->bindParam(":hint", $this->hint);
        $stmt->bindParam(":fe_id", $this->fe_id);
    
        // execute query
        if ($stmt->execute())
            return true;
    
        return false;
    }

    // delete the code
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE fe_id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->fe_id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->fe_id);
    
        // execute query
        if($stmt->execute())
            return true;
    
        return false;
    }

    // delete the bookmark
    function deleteMark($user_id, $fe_id){
    
        // delete query
        $query = "DELETE FROM
                    code_bookmarks
                WHERE
                    `user_id` = :user_id
                AND
                    `code_id` = (SELECT MAX(`id`) FROM codes WHERE `fe_id` = :fe_id)";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $user_id=htmlspecialchars(strip_tags($user_id));
        $fe_id=htmlspecialchars(strip_tags($fe_id));

        // bind values
        $stmt->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam(":fe_id", $fe_id);
    
        // execute query
        if($stmt->execute())
            return true;
    
        return false;
    }

    // search codes
    function search($keyword, $noItems, $long, $lat){
    
        // select all query
        $query = "SELECT
                    (ABS(`long` - :long) + ABS(`lat` - :lat)) as distance, `fe_id`, `cat_id`, `title`, `long`, `lat`,
                    (SELECT
                        IFNULL(AVG(cr.fun), 0)
                    FROM
                        code_ratings cr
                    WHERE
                        cr.code_id = c.id) as fun,
                    (SELECT
                        IFNULL(AVG(cr.dif), 0)
                    FROM
                        code_ratings cr
                    WHERE
                        cr.code_id = c.id) as dif
                FROM
                    " . $this->table_name . " c
                WHERE
                    `title` LIKE :key OR `desc` LIKE :key OR `hint` LIKE :key
                ORDER BY
                    distance ASC, fe_id
                LIMIT
                    :items";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keyword=htmlspecialchars(strip_tags($keyword));
        $noItems=htmlspecialchars(strip_tags($noItems));
        $long=htmlspecialchars(strip_tags($long));
        $lat=htmlspecialchars(strip_tags($lat));
        $keyword="%{$keyword}%";
    
        // bind params
        $stmt->bindParam(':long', $long, PDO::PARAM_INT);
        $stmt->bindParam(':lat', $lat, PDO::PARAM_INT);
        $stmt->bindParam(':key', $keyword);
        $stmt->bindParam(':items', $noItems, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // check if fe_id already exists
    public function checkFeid(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " WHERE `fe_id` = ?";
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->fe_id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }

    // check if rand_id already exists
    public function checkRandid(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " WHERE `rand_id` = ?";
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->rand_id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }

    // check if code is bookmarked
    public function checkMark($user_id){
        $query = "SELECT COUNT(*) as total_rows FROM code_bookmarks WHERE `user_id` = :user_id AND `code_id` = :code_id";
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':code_id', $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return boolval($row['total_rows']);
    }

    // used for paging codes
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }
}
?>