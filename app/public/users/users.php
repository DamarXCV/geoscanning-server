<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate users object
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// The request is using the GET method
// read data belonging to an existing user
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // read GET data
    $data = (object) $_GET;

    // user has active token?
    if($user->checkToken($user->auth())) {
        // check if transmited data is empty
        if (!empty($data->user_id)) {

            $user->id = $data->user_id;
    
            // read current user data
            $user->read();
    
            // set response code - 200 OK
            http_response_code(200);
    
            // return read user
            echo json_encode(array(
                "user_id"          => intval($user->id),
                "alias"           => $user->alias,
                "joined_at"       => $user->joined,
                "country"         => $user->country,
                "hometown"        => $user->hometown,
                "description"     => $user->desc,
                "profile_image"   => $user->profile_image,
                "no_hidden_codes" => intval($user->codes_hidden),
                "no_found_codes"  => intval($user->codes_found)
            ));
        } else {
            // set response code - 403 Forbidden
            http_response_code(403);
        }
    }
}

// The request is using the POST method
// insert a new user
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = (object) $_POST;
    // make sure data is not empty
    if(
        !empty($data->password) &&
        !empty($data->firstname) &&
        !empty($data->lastname) &&
        !empty($data->alias) &&
        !empty($data->hometown) &&
        !empty($data->country) &&
        !empty($data->email)/* &&
        !empty($data->desc) &&
        !empty($data->profile_image)*/
    ){
    
        // set user property values
        $user->pw          = $user->hashPw($data->password);
        $user->fname       = $data->firstname;
        $user->lname       = $data->lastname;
        $user->alias       = $data->alias;
        $user->email       = $data->email;
        $user->joined      = date('Y-m-d H:i:s');
        $user->country     = $data->country;
        $user->hometown    = $data->hometown;
        $user->desc        = $data->desc;
        $user->profile_image=$data->profile_image;

        // create the user
        if($user->create()){
            $token = $user->createToken();
            if($token){
                // set response code - 201 created
                http_response_code(201);
                    
                // return created user data
                echo json_encode(array(
                    "token"         => $token,
                    "user_id"       => intval($user->id),
                    "alias"         => $user->alias,
                    "joined_at"     => $user->joined,
                    "country"       => $user->country,
                    "hometown"      => $user->hometown,
                    "description"   => $user->desc,
                    "profile_image" => $user->profile_image
                ));
            }
        }
    
        // if unable to create the user, tell the user
        else{
    
            // set response code - 503 service unavailable
            http_response_code(503);
    
            // tell the user
            echo json_encode(array("message" => "Unable to create code."));
        }
    }
    
    // tell the user data is incomplete
    else{
    
        // set response code - 400 bad request
        http_response_code(400);
    
        // tell the user
        echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
    }
}

// The request is using the PATCH method
elseif ($_SERVER['REQUEST_METHOD'] === 'PATCH') {
    parse_str(file_get_contents('php://input'), $data);
    $data = (object) $data;

    // user has active token
    if($user->checkToken($user->auth())) {

        // make sure data is not empty
        if(
            !empty($data->firstname) &&
            !empty($data->lastname) &&
            !empty($data->alias) &&
            !empty($data->hometown) &&
            !empty($data->country) &&
            !empty($data->email)/* &&
            !empty($data->desc) &&
            !empty($data->profile_image)*/
        ){
            // set user property values
            $user->fname       = $data->firstname;
            $user->lname       = $data->lastname;
            $user->alias       = $data->alias;
            $user->email       = $data->email;
            $user->country     = $data->country;
            $user->hometown    = $data->hometown;
            $user->desc        = $data->desc;
            $user->profile_image=$data->profile_image;
            
            // update the user
            if($user->update()){

                // set response code - 200 OK
                http_response_code(200);
        
                // return updated object
                echo json_encode(array(
                    "user_id"        => intval($user->id),
                    "alias"         => $user->alias,
                    "joined_at"     => $user->joined,
                    "country"       => $user->country,
                    "hometown"      => $user->hometown,
                    "description"   => $user->desc,
                    "profile_image" => $user->profile_image
                ));
            }
        
            // if unable to update the user, tell the user
            else{
        
                // set response code - 204 no content
                http_response_code(204);
        
                // tell the user
                echo json_encode(array("message" => "no content."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to update code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the DELETE method
elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        // set user id to be deleted
        $user->id = $data->user_id;
        
        // delete the user
        if($user->delete()){
        
            // set response code - 200 ok
            http_response_code(200);
        
            // tell the user
            echo json_encode(array("message" => "User was deleted."));
        }
        
        // if unable to delete the user
        else{
        
            // set response code - 503 service unavailable
            http_response_code(503);
        
            // tell the user
            echo json_encode(array("message" => "Unable to delete user."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
    
}

// The request is not using any known method
else {
    return false;
}

?>