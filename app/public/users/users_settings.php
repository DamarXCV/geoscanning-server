<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate users object
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// The request is using the GET method
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if($user->checkToken($user->auth())) {

        // read user data matching to token
        $user->read();

        // set response code - 200 OK
        http_response_code(200);

        // return read user
        echo json_encode(array(
            "id"              => intval($user->id),
            "firstname"       => $user->fname,
            "lastname"        => $user->lname,
            "alias"           => $user->alias,
            "email"           => $user->email,
            "joined_at"       => $user->joined,
            "country"         => $user->country,
            "hometown"        => $user->hometown,
            "description"     => $user->desc,
            "profile_image"   => $user->profile_image,
            "no_hidden_codes" => intval($user->codes_hidden),
            "no_found_codes"  => intval($user->codes_found)
        ));
    }
}

// The request is not using any known method
else {
    return false;
}

?>