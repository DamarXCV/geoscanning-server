<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/codes.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$code = new Code($db);
$user = new User($db);

// The request is using the GET method
// get codes bookmarked by user
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        if (//!empty($data->user_id) &&
            !empty($data->before_date) &&
            //!empty($data->start_position) &&
            !empty($data->number_of_items)) {

            //$user_id = $data->user_id;
            $age     = $data->before_date;
            $start   = $data->start_position;
            $noItems = $data->number_of_items;

            $stmt = $code->readMarks($data->user_id, $age, $start, $noItems);
            $num = $stmt->rowCount();

            // check if more than 0 record found
            if($num>0){
            
                // codes array
                $code_arr=array();
            
                // retrieve our table contents
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    extract($row);
            
                    $code_item=array(
                        "id"         => $fe_id,
                        "category"   => intval($cat_id),
                        "title"      => $title,
                        "long"       => floatval($long),
                        "lat"        => floatval($lat),
                        "dif"        => floatval($dif),
                        "fun"        => floatval($fun)
                    );
            
                    array_push($code_arr, $code_item);
                }
            
                // set response code - 200 OK
                http_response_code(200);
            
                // show code data in json format
                echo json_encode($code_arr);
            } else {
            
                // set response code - 204 No Data
                http_response_code(204);
            
                // tell the user no codes found
                echo json_encode(
                    array("message" => "No codes found.")
                );
            }
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}


// The request is using the POST method
// bookmark a code
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = (object) $_POST;
    if($user->checkToken($user->auth())) {
        // make sure data is not empty
        if(!empty($data->code_id)){

            // create the bookmark
            if($code->createMark($user->id, $data->code_id)){
        
                // set response code - 201 created
                http_response_code(201);
        
                // return created object
                echo json_encode(array(
                    "user_id" => $user->id,
                    "code_id" => $data->code_id
                ));
            }
        
            // if unable to create the bookmark, tell the user
            else{
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("message" => "Unable to create bookmark."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the DELETE method
// delete a bookmark
elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {

        if(!empty($data->code_id)){

            // create the bookmark
            if($code->deleteMark($user->id, $data->code_id)){
        
                // set response code - 200 ok
                http_response_code(200);
        
                // tell the user
                echo json_encode(array("message" => "Bookmark was deleted."));
            }
        
            // if unable to delete the bookmark, tell the user
            else{
            
                // set response code - 503 service unavailable
                http_response_code(503);
            
                // tell the user
                echo json_encode(array("message" => "Unable to delete bookmark."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to delete bookmark. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    return false;
}

?>