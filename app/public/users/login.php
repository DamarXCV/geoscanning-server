<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("charset=UTF-8");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate users object
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

// instantiate users object
$user = new User($db);

// The request is using the POST method
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // retrieve transmitted data
    $data = (object) $_POST;
    // make sure data is not empty
    if(
        !empty($data->email) &&
        !empty($data->password)
    ){
    
        // set user property values
        $user->email = $data->email;

        // create the user
        if($user->checkPw($data->password)){
    
            $token = $user->createToken();

            // set response code - 200 OK
            http_response_code(200);
    
            // return created object
            echo json_encode(array(
                "user_id" => intval($user->id),
                "token"   => $token
            ));
        } else {
    
            // set response code - 403 Forbidden
            http_response_code(403);
    
            echo json_encode(array("message" => "Wrong password."));
        }
    }
    
    // tell the user data is incomplete
    else{
    
        // set response code - 400 bad request
        http_response_code(400);
    
        // tell the user
        echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
    }
}

// The request is not using any known method
else {
    return false;
}

?>