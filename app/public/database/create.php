<?php

include_once '../config/database.php';

$database = new Database();
$db = $database->getConnection();

$query = "DROP TABLE IF EXISTS `sessions`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DROP TABLE IF EXISTS `code_bookmarks`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DROP TABLE IF EXISTS `code_ratings`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DROP TABLE IF EXISTS `codes`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DROP TABLE IF EXISTS `users`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DROP TABLE IF EXISTS `categories`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "CREATE TABLE IF NOT EXISTS `categories` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */

$query = "CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `pwhash` char(60) NOT NULL,
    `fname` varchar(128) NOT NULL,
    `lname` varchar(128) NOT NULL,
    `alias` varchar(256) NOT NULL UNIQUE,
    `email` varchar(256) NOT NULL UNIQUE,
    `joined` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `country` varchar(256),
    `hometown` varchar(256),
    `desc` varchar(1024),
    `profile_image` blob,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */

$query = "CREATE TABLE IF NOT EXISTS `codes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `fe_id` varchar(8) NOT NULL UNIQUE,
    `rand_id` varchar(40) NOT NULL UNIQUE,
    `user_id` int(11) NOT NULL,
    `cat_id` int(11) NOT NULL,
    `hidden` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `title` varchar(256),
    `long` float(10,7),
    `lat` float(9,7),
    `desc` varchar(1024),
    `hint` varchar(256),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES users(`id`),
    FOREIGN KEY (`cat_id`) REFERENCES categories(`id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */

$query = "CREATE TABLE IF NOT EXISTS `code_ratings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `code_id` int(11) NOT NULL,
    `found` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `dif` int(11),
    `fun` int(11),
    `comment` varchar(1024),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES users(`id`),
    FOREIGN KEY (`code_id`) REFERENCES codes(`id`),
    UNIQUE KEY `rating_id` (`user_id`, `code_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */

$query = "CREATE TABLE IF NOT EXISTS `code_bookmarks` (
    `user_id` int(11) NOT NULL,
    `code_id` int(11) NOT NULL,
    FOREIGN KEY (`user_id`) REFERENCES users(`id`),
    FOREIGN KEY (`code_id`) REFERENCES codes(`id`),
    UNIQUE KEY `bookmark_id` (`user_id`, `code_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */

$query = "CREATE TABLE IF NOT EXISTS `sessions` (
    `user_id` int(11) NOT NULL,
    `token` varchar(256) NOT NULL,
    `valid_until` datetime NOT NULL DEFAULT DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 1000 YEAR),
    FOREIGN KEY (`user_id`) REFERENCES users(`id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$stmt = $db->prepare($query);

$stmt->execute();

/* INSERT */