<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/codes.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$code = new Code($db);
$user = new User($db);

// The request is using the GET method
// generate a qr code
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        if (!empty($data->code_id)) {

            $code->fe_id = $data->code_id;
            
            $code->readRandId($user->id);
            if($code->rand_id){
                $url = "https://chart.googleapis.com/chart" .
                        "?chs=250x250&cht=qr&chl=" .
                        $code->rand_id .
                        "&choe=UTF-8";
                $qr = file_get_contents($url);

                //echo '<img src="data:image/png;base64,' . base64_encode($qr) . '">';

                
                // set response code - 200 OK
                http_response_code(200);
                
                // return read code
                echo json_encode(array("data" => base64_encode($qr)));
            } else {
                // wrong user
                // set response code - 403 forbidden
                http_response_code(403);
            }
        }
    } else {
        // no valid login
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    //return false;
}

?>