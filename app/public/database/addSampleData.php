<?php

include_once '../config/database.php';
  
$database = new Database();
$db = $database->getConnection();

$query = "DELETE FROM `code_bookmarks`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DELETE FROM `code_ratings`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DELETE FROM `codes`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DELETE FROM `users`";
$stmt = $db->prepare($query);
$stmt->execute();

$query = "DELETE FROM `categories`";
$stmt = $db->prepare($query);
$stmt->execute();

$query =
    "INSERT INTO `categories`
        (`name`)
    VALUES
        ('default'),
        ('regular'),
        ('multi'),
        ('lost place'),
        ('mystery')";

$stmt = $db->prepare($query);

$stmt->execute();

//-------------------

$query =
    "INSERT INTO `users`
        (`fname`, `lname`, `alias`, `email`, `country`, `hometown`, `desc`, `pwhash`)
    VALUES
        ('Thomas', 'Lokomotive', 'TL', 'thomas.dielokomotive@gmail.com', 'Deutschland', 'München', 'Ich bin Thomas die Lokomotive.', '\$2y\$10\$m3vtC5geuXcZTP9W1AfE4eL2LNfdF2tRDp2FdIRZ/han/41A/BQb.'/*krassesPW*/),
        ('Andreas', 'Baum', 'Baummann', 'baummann@hotmail.de', 'Deutschland', 'Berlin', 'Baummann schreitet zur Tat!', '\$2y\$10\$olgUmH/Mnmkiy2RU1hkKueCHSq/CRzgxflgM7ci1HkKWIJ1c9m19u'/*Baum*/),
        ('Kevin', 'Müller', 'Kevin', 'kevin89@web.de', 'Deutschland', 'Halle', 'Ein Kevin.', '\$2y\$10\$YjmTkw5VXKTc5puV6j8xVuJP/Ms8Q3BFb.tumcXZ48OO1iX2RQIwG'/*secret*/),
        ('Leopold', 'Nilsson', 'Hackerman', 'hackerman@aol.com', 'USA', 'Miami', 'The most powerful hacker of all time.', '\$2y\$10\$wQ1tDhNKTNs.wMYV0qKg2.w.PQQaTevxsCscOWbhW8pHeFg0ZORYi'/*|\|0T_/-\_|°\/\/*/),
        ('David', 'Sandberg', 'Kung Fury', 'KF0815@gmail.com', 'USA', 'Miami', '', '\$2y\$10\$ay2SVXeMXFPGoIcJTZfk6u2eeCVpAOmYYyPs7nrunQseT/fZKG4p2'/*Fury4711*/)";

$stmt = $db->prepare($query);

$stmt->execute();

//-------------------

$query =
    "INSERT INTO `codes`
        (`fe_id`, `rand_id`, `user_id`, `cat_id`, `title`, `lat`, `long`, `desc`, `hint`)
    VALUES
        ('GC8A29F4', '77F494B71C1542E495378761A39092133B944D38',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'regular'),
            'Erster Code', '50.990172', '11.056001', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GCC3413D', 'F9763A10294B51DC78740894A63932A288E82D25',
            (SELECT id from users WHERE alias = 'Baummann'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Noch ein Code', '50.998767', '11.048976', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E30', '645B82FF5443E077947C22276C0862822513AD0B',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der dritte Code', '50.984957', '11.042987', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E31', '645B82FF5443E077947C22276C0862822513AD0C',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 4. Code', '50.976186', '11.027929', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E32', '645B82FF5443E077947C22276C0862822513AD0D',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 5. Code', '50.977024', '11.027779', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E33', '645B82FF5443E077947C22276C0862822513AD0E',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 6. Code', '50.978098', '11.024088', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E34', '645B82FF5443E077947C22276C0862822513AD0F',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 7. Code', '50.991740', '11.015003', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E35', '645B82FF5443E077947C22276C0862822513AD10',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 8. Code', '51.016923', '10.994468', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E36', '645B82FF5443E077947C22276C0862822513AD11',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 9. Code', '51.024401', '10.975814', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E37', '645B82FF5443E077947C22276C0862822513AD12',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 10. Code', '50.976098', '11.023139', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E38', '645B82FF5443E077947C22276C0862822513AD13',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 11. Code', '50.974619', '11.033728', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E39', '645B82FF5443E077947C22276C0862822513AD14',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 12. Code', '50.976145', '11.032237', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3A', '645B82FF5443E077947C22276C0862822513AD15',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 13. Code', '50.975659', '10.952972', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3B', '645B82FF5443E077947C22276C0862822513AD16',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 14. Code', '51.0', '11.0', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3C', '645B82FF5443E077947C22276C0862822513AD17',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 15. Code', '51.023943', '11.030363', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3D', '645B82FF5443E077947C22276C0862822513AD18',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 16. Code', '51.023092', '11.022724', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3E', '645B82FF5443E077947C22276C0862822513AD19',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 17. Code', '51.037323', '11.078771', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E3F', '645B82FF5443E077947C22276C0862822513AD1A',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 18. Code', '50.96', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E40', '645B82FF5443E077947C22276C0862822513AD1B',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 19. Code', '50.97', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E41', '645B82FF5443E077947C22276C0862822513AD1C',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 20. Code', '50.98', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E42', '645B82FF5443E077947C22276C0862822513AD1D',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 21. Code', '50.99', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E43', '645B82FF5443E077947C22276C0862822513AD1E',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 22. Code', '51.0', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E44', '645B82FF5443E077947C22276C0862822513AD1F',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 23. Code', '51.01', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E45', '645B82FF5443E077947C22276C0862822513AD20',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 24. Code', '51.02', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'),
        ('GC437E46', '645B82FF5443E077947C22276C0862822513AD21',
            (SELECT id from users WHERE alias = 'Hackerman'),
            (SELECT id from categories WHERE name = 'mystery'),
            'Schon der 25. Code', '51.03', '11.03', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam')";

$stmt = $db->prepare($query);

$stmt->execute();

//-------------------

$query =
    "INSERT INTO `code_ratings`
        (`user_id`, `code_id`, `dif`, `fun`, `comment`)
    VALUES
        ((SELECT id FROM users WHERE alias = 'Hackerman'),
            (SELECT id FROM codes WHERE fe_id = 'GCC3413D'),
            1, 1, '01010110 01101001 01100101 01101100 00100000 01111010 01110101 00100000 01100101 01101001 01101110 01100110 01100001 01100011 01101000 00101100 00100000 01100101 01111001'),
        ((SELECT id FROM users WHERE alias = 'Baummann'),
            (SELECT id FROM codes WHERE fe_id = 'GC8A29F4'),
            4, 5, 'FTF beim ersten Code. Bin voll gehyped.'),
        ((SELECT id FROM users WHERE alias = 'Kevin'),
            (SELECT id FROM codes WHERE fe_id = 'GC8A29F4'),
            3, 4, 'Verdammt, Baummann war schneller als ich :('),
        ((SELECT id FROM users WHERE alias = 'Kung Fury'),
            (SELECT id FROM codes WHERE fe_id = 'GCC3413D'),
            5, 5, 'Keine Lust mehr, mir Kommentare auszudenken.'),
        ((SELECT id FROM users WHERE alias = 'Kung Fury'),
            (SELECT id FROM codes WHERE fe_id = 'GC437E30'),
            4, 3, 'Also echt jetzt.')";

$stmt = $db->prepare($query);

$stmt->execute();

//-------------------

$query =
    "INSERT INTO `code_bookmarks`
        (`user_id`, `code_id`)
    VALUES
        ((SELECT id FROM users WHERE alias = 'TL'),        (SELECT id FROM codes WHERE fe_id = 'GC8A29F4')),
        ((SELECT id FROM users WHERE alias = 'Kung Fury'), (SELECT id FROM codes WHERE fe_id = 'GC8A29F4')),
        ((SELECT id FROM users WHERE alias = 'TL'),        (SELECT id FROM codes WHERE fe_id = 'GCC3413D')),
        ((SELECT id FROM users WHERE alias = 'Kevin'),     (SELECT id FROM codes WHERE fe_id = 'GCC3413D')),
        ((SELECT id FROM users WHERE alias = 'TL'),        (SELECT id FROM codes WHERE fe_id = 'GC437E30')),
        ((SELECT id FROM users WHERE alias = 'Kevin'),     (SELECT id FROM codes WHERE fe_id = 'GC437E30'))";
$stmt = $db->prepare($query);

$stmt->execute();

//-------------------

?>