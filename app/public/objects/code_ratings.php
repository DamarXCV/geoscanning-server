<?php
class CodeRating{
    // database connection and table name
    private $conn;
    private $table_name = "code_ratings";

    // object properties
    public $id;
    public $user_id;
    public $user_name;
    public $code_fe_id;
    public $code_id;
    public $found;
    public $dif;
    public $fun;
    public $comment;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read single code rating
    function readOne(){
    
        // query to read single record
        $query =
            "SELECT
                count(*) as c, cr.id, u.alias, c.fe_id, cr.found, IFNULL(cr.dif, 0) as dif, IFNULL(cr.fun, 0) as fun, cr.comment
            FROM
                " . $this->table_name . " cr
                LEFT JOIN
                    users u
                        ON cr.user_id = u.id
                LEFT JOIN
                    codes c
                        ON cr.code_id = c.id
                WHERE
                    cr.user_id = :user_id
                AND
                    cr.code_id = :code_id
                LIMIT
                    1";

        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // bind id of code to be updated
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->bindParam(':code_id', $this->code_id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if(!$row['c'])
            return false;
            
        // set values to object properties
        $this->id       = $row['id'];
        $this->user_name= $row['alias'];
        $this->code_fe_id=$row['fe_id'];
        $this->found    = $row['found'];
        $this->dif      = $row['dif'];
        $this->fun      = $row['fun'];
        $this->comment  = html_entity_decode($row['comment']);
        return true;
    }

    // read all code ratings of geven code
    function read($code_id, $age, $start, $noItems){

        // select all query
        $query =
            "SELECT
                cr.id, cr.user_id, u.alias, cr.found, IFNULL(cr.dif, 0) as dif, IFNULL(cr.fun, 0) as fun, cr.comment
            FROM
                " . $this->table_name . " cr
            LEFT JOIN
                users u
                    ON cr.user_id = u.id
            LEFT JOIN
                codes c
                    ON cr.code_id = c.id
            WHERE
                c.fe_id = :code_id
            AND
                found < :found
            ORDER BY
                cr.found
            LIMIT
                :start, :noItems";
        
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $this->hint=htmlspecialchars(strip_tags($code_id));
        $this->hint=htmlspecialchars(strip_tags($age));
        $this->hint=htmlspecialchars(strip_tags($start));
        $this->hint=htmlspecialchars(strip_tags($noItems));

        // bind values
        $stmt->bindParam(":code_id", $code_id);
        $stmt->bindParam(":found",   $age);
        $stmt->bindParam(":start",   $start,   PDO::PARAM_INT);
        $stmt->bindParam(":noItems", $noItems, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();

        return $stmt;
    }

    // create code rating
    function create(){
    
        $query = "SELECT id FROM codes WHERE fe_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->code_fe_id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->code_id = $row['id'];

        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                        (`user_id`, `code_id`, `dif`, `fun`, `comment`)
                VALUES
                    (:user_id, :code_id, :dif, :fun, :comment)";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->code_id=htmlspecialchars(strip_tags($this->code_id));
        $this->dif=htmlspecialchars(strip_tags($this->dif));
        $this->fun=htmlspecialchars(strip_tags($this->fun));
        $this->comment=htmlspecialchars(strip_tags($this->comment));
    
        // bind new values
        $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
        $stmt->bindParam(':code_id', $this->code_id, PDO::PARAM_INT);
        $stmt->bindParam(':dif', $this->dif);
        $stmt->bindParam(':fun', $this->fun);
        $stmt->bindParam(':comment', $this->comment);
    
        // execute the query
        if($stmt->execute()){
            $this->readOne();
            return true;
        }
    
        return false; 
    }

    // create empty code rating
    // used when scanning a code
    function createEmpty(){

        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                        (`user_id`, `code_id`)
                VALUES
                    (:user_id, :code_id)";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->code_id=htmlspecialchars(strip_tags($this->code_id));
    
        // bind new values
        $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
        $stmt->bindParam(':code_id', $this->code_id, PDO::PARAM_INT);
    
        // execute the query
        if($stmt->execute()){
            $this->readOne();
            return true;
        }
    
        return false; 
    }

    // update a code rating
    function update(){
    
        // get code id from code fe id
        $query = "SELECT id FROM codes WHERE fe_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->code_fe_id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->code_id = $row['id'];

        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    dif     = :dif,
                    fun     = :fun,
                    comment = :comment
                WHERE
                    code_id = :code_id
                AND
                    user_id = :user_id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->code_id=htmlspecialchars(strip_tags($this->code_id));
        $this->dif=htmlspecialchars(strip_tags($this->dif));
        $this->fun=htmlspecialchars(strip_tags($this->fun));
        $this->comment=htmlspecialchars(strip_tags($this->comment));
    
        // bind new values
        $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
        $stmt->bindParam(':code_id', $this->code_id, PDO::PARAM_INT);
        $stmt->bindParam(':dif', $this->dif);
        $stmt->bindParam(':fun', $this->fun);
        $stmt->bindParam(':comment', $this->comment);
    
        // execute the query
        if($stmt->execute()){
            // get all infos
            $this->readOne();
            return true;
        }
    
        return false;
    }

    // search code via rand id
    // used to log a code
    function searchKey($key){
    
        // select all query
        $query = "SELECT
                    id
                FROM
                    codes
                WHERE
                    rand_id = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $key=htmlspecialchars(strip_tags($key));
    
        // bind
        $stmt->bindParam(1, $key);
    
        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        if ($row['id']) {
            $this->code_id = $row['id'];
            return true;
        }
    
        return false;
    }

    // used for paging code ratings
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }
}
?>