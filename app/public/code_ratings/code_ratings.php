<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
//header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate codes object
include_once '../objects/code_ratings.php';
// instantiate users object used for authentication
include_once '../objects/users.php';

$database = new Database();
$db = $database->getConnection();

$codeRating = new CodeRating($db);
$user = new User($db);

// The request is using the GET method
// get code ratings for given code
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = (object) $_GET;
    if($user->checkToken($user->auth())) {
        if (!empty($data->code_id) &&
            !empty($data->before_date) &&
            //!empty($data->start_position) &&
            !empty($data->number_of_items)) {

            $code_id  = $data->code_id;
            $age      = $data->before_date;
            $startPos = $data->start_position;
            $noItems  = $data->number_of_items;

            $stmt = $codeRating->read($code_id, $age, $startPos, $noItems);
            $num = $stmt->rowCount();

            if($num>0){
            
                // codes array
                $rating_arr=array();
            
                // retrieve our table contents
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    extract($row);
            
                    $rating_item=array(
                        "id"         => intval($id),
                        "owner_id"   => intval($user_id),
                        "owner_name" => $alias,
                        "date_found" => $found,
                        "dif"        => floatval($dif),
                        "fun"        => floatval($fun),
                        "comment"    => $comment
                    );
            
                    array_push($rating_arr, $rating_item);
                }
            
                // set response code - 200 OK
                http_response_code(200);
            
                // show code data in json format
                echo json_encode($rating_arr);
            } else {
            
                // set response code - 204 No Data
                http_response_code(204);
            
                // tell the user no codes found
                echo json_encode(
                    array("message" => "No codes found.")
                );
            }
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the POST method
// create a code rating
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = (object) $_POST;
    if($user->checkToken($user->auth())) {
        // make sure data is not empty
        if(
            !empty($data->code_id) &&
            !empty($data->dif) &&
            !empty($data->fun)/* &&
            !empty($data->comment)*/
        ){
        
            // set code property values
            $codeRating->code_fe_id=$data->code_id;
            $codeRating->user_id  = $user->id;
            $codeRating->user_name= $user->alias;
            $codeRating->dif      = $data->dif;
            $codeRating->fun      = $data->fun;
            $codeRating->comment  = $data->comment;

            // create the code
            if($codeRating->create()){
        
                // set response code - 201 created
                http_response_code(201);
        
                // return created object
                echo json_encode(array(
                    "id"         => $codeRating->code_fe_id,
                    "owner_id"   => intval($codeRating->user_id),
                    "owner_name" => $codeRating->user_name,
                    "date_found" => $codeRating->found,
                    "dif"        => floatval($codeRating->dif),
                    "fun"        => floatval($codeRating->fun),
                    "comment"    => $codeRating->comment
                ));
            }
        
            // if unable to create the code, tell the user
            else{
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("message" => "Unable to create code."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to create code. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is using the PATCH method
// update a code rating
elseif ($_SERVER['REQUEST_METHOD'] === 'PATCH') {

    parse_str(file_get_contents('php://input'), $data);
    $data = (object) $data;
    if($user->checkToken($user->auth())) {

        // make sure data is not empty
        if(
            !empty($data->code_id) &&
            !empty($data->dif) &&
            !empty($data->fun)/* &&
            !empty($data->comment)*/
        ){
        
            // set code property values
            $codeRating->code_fe_id=$data->code_id;
            $codeRating->user_id  = $user->id;
            $codeRating->user_name= $user->alias;
            $codeRating->dif      = $data->dif;
            $codeRating->fun      = $data->fun;
            $codeRating->comment  = $data->comment;

            // create the code
            if($codeRating->update()){
        
                // set response code - 200 OK
                http_response_code(200);
        
                // return created object
                echo json_encode(array(
                    "id"         => $codeRating->code_fe_id,
                    "owner_id"   => intval($codeRating->user_id),
                    "owner_name" => $codeRating->user_name,
                    "date_found" => $codeRating->found,
                    "dif"        => floatval($codeRating->dif),
                    "fun"        => floatval($codeRating->fun),
                    "comment"    => $codeRating->comment
                ));
            }
        
            // if unable to create the code, tell the user
            else{
        
                // set response code - 204 no content
                http_response_code(204);
        
                // tell the user
                echo json_encode(array("message" => "no content."));
            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("message" => "Unable to update code rating. Data is incomplete."));
        }
    } else {
        // set response code - 403 Forbidden
        http_response_code(403);
    }
}

// The request is not using any known method
else {
    return false;
}

?>